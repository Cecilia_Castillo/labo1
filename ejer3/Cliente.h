#ifndef CLIENTE_H_
#define CLIENTE_H_
#include <iostream>
#include <stdlib.h>
using namespace std;

class Cliente {
    private:
        int cantidad = 0;
        string *nombre = NULL;
        string *telefono = NULL;
        int *saldo = 0;
        bool *moroso = NULL;

    public:
        /* constructores */
        Cliente();
        Cliente (int, string *nombre, string, int, bool);
        
         /* métodos get and set */
        int get_cantidad();
        string get_nombre();
        string get_telefono();
        int get_saldo();
        bool get_moroso();
        
        void set_cantidad(int cantidad);
        void set_nombre(int cantidad, string *nombre);
        void set_telefono(int cantidad, string *telefono);
        void set_saldo(int cantidad, int saldo);
        void set_moroso(int cantidad, bool moroso);

};
#endif /* CLIENTE_H*/
