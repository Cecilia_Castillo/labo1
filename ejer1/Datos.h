#ifndef DATOS_H_
#define DATOS_H_

class Datos {
    private:
        int cantidad = 0;
        int *numeros = 0;

    public:
        /* constructores */
        Datos ();
        Datos (int cantidad, int numeros);
        
         /* métodos get and set */
        int get_cantidad();
        int get_numeros();
        void set_cantidad(int cantidad);
        void set_numeros(int numeros);

};
#endif /* DATOS_H*/

