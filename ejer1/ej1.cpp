#include <iostream>
#include <cmath>
using namespace std;
#include "Datos.h"


int cantidad, *numeros;
int cuadrados[]={};
int suma=0;

//En esta funciòn se ingresan los datos para el arreglo
void vector_cantidad(){
	cout << "Ingrese un numero para la cantidad de numeros: ";
	cin >> cantidad;
	numeros = new int [cantidad];
	for(int i=0; i<cantidad; i++){
		cout << "Ingrese un numero: ";
		cin >> numeros[i];
		}
	
}

//Se calcula el cuadrado de los numeros ingresados al array
void cuadrado(){
	cout << "Cuadrados: ";
	for (int i=0; i<cantidad; i++){
		cuadrados[i]=numeros[i];
		//pow corresponde a una funcion de la libreria cmath
		//permite elevar a cualquier potencia
		cuadrados[i]=pow(cuadrados[i], 2);
		cout << cuadrados[i] << " ";
		}
	int suma=0;	
	for (int i=0; i<cantidad; i++){

		suma = suma + cuadrados[i];
		cout << "Suma de cuadrados: " << suma << endl;
		}
	
}

void impresora(){
	cout << "Numeros: ";
	for (int i=0; i<cantidad; i++){
		cout << numeros[i] << " ";
		}

}
//solo llama a las funciones
int main(int argc, char **argv)
{
	//int cantidad = atoi (argv[1]);
	vector_cantidad();
	impresora();
	cuadrado();
	delete[] numeros;

	return 0;
}

