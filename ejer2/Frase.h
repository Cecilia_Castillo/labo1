#ifndef FRASE_H_
#define FRASE_H_
#include <iostream>
#include <stdlib.h>
using namespace std;

class Frase {
    private:
        int cantidad = 0;
        string *frases = NULL;

    public:
        /* constructores */
        Frase ();
        Frase (int cantidad, string *frases);
        
         /* métodos get and set */
        int get_cantidad();
        string get_frases();
        void set_cantidad(int cantidad);
        void set_frases(int cantidad, string *frases);
        
        
        void contador(int cantidad, string *frases);

};
#endif /* FRASE_H*/
