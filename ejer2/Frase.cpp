#include <iostream>
using namespace std;
#include "Frase.h"


/*Constructor*/
Frase::Frase() {
	int cantidad= 0;
	string *frases = NULL;
}

Frase::Frase(int cantidad, string *frases){
	this->cantidad = cantidad;
	this->frases = frases;
	
}

int Frase::get_cantidad(){
	return this->cantidad;
}

string Frase::get_frases(){
	return this->frases[cantidad];
}

void Frase::set_cantidad(int cantidad){
	this->cantidad = cantidad;

}

void Frase::set_frases(int cantidad, string *frases){
	this->frases = frases;
}


/*
 * Contador es una funciòn con un for anidado, en el primer for se recorre
 * el arreglo, en el segundo se encuentran las condiciones 
 * de si la letra es mayùscula o no
 */
void Frase::contador(int cantidad, string *frases){
	int cont_mayus=0;
	int cont_minus=0;
	
	for(int i=0; i<cantidad; i++){
		int largo = frases[i].length;
		//cout << frases[i] << endl;
		for(int j=0; j<largo; j++){
			//condiciòn para mayùsculas
			if(isupper(frases[i][j])){
				cont_mayus++;
				}
				//condiciòn para minùsculas
			else if(islower(frases[i][j])){
				cont_minus++;
				}
		}
	}
	cout << "Total mayusculas: " << cont_mayus << endl;
	cout << "Total minusculas: " << cont_minus << endl;
}
